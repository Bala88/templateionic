import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceduresPage } from './procedures.page';

describe('ProceduresPage', () => {
  let component: ProceduresPage;
  let fixture: ComponentFixture<ProceduresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceduresPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceduresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
