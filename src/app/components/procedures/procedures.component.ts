import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-procedures',
  templateUrl: './procedures.component.html',
  styleUrls: ['./procedures.component.scss'],
})
export class ProceduresComponent implements OnInit {
  proceduresList:any;
  constructor(private searchService:SearchService,
    public router:Router) { }

  ngOnInit() {}
  // getProcedures(){
  //   console.log("triggered")
  // }
  getProcedures(ev){
    let key =ev.target.value;
    this.searchService.getProceduresList(key).subscribe(
      res => {
        this.proceduresList=res;
        console.log(this.proceduresList)
      });
   
  }
  navigate(item){
  console.log(item)
  this.router.navigateByUrl('/tabs/(procedure-list:procedure-list)');
  }

}
