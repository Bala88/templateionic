import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


export const BASE_URL="http://localhost:8086/labespy/procedures/";
export const DOCTORS_BASE_URL="http://localhost:8086/labespy/doctors/";
export const DRUGS_BASE_URL="http://localhost:8086/labespy/drugs/";
export const LABS_BASE_URL="http://localhost:8086/labespy/labs/";


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(public http:HttpClient) { }

  getProceduresList(key):Observable<any>{
    return this.http.get(BASE_URL+"search/"+key);
   }
   getDoctorsList(key){
     return this.http.get(DOCTORS_BASE_URL+key);
   }
   getDrugList(key){
     return this.http.get(DRUGS_BASE_URL+key);
   }
   getLabsList(key){
     return this.http.get(LABS_BASE_URL+key);
   }
}
