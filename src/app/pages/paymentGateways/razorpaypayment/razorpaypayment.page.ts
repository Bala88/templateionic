/**
* Ionic Full App  (https://store.enappd.com/product/ionic-full-app-ionic-4-full-app)
*
* Copyright © 2019-present Enappd. All rights reserved.
*
* This source code is licensed as per the terms found in the
* LICENSE.md file in the root directory of this source tree.
*/

import { Component, OnInit } from '@angular/core';
declare var RazorpayCheckout: any;
@Component({
  selector: 'app-razorpaypayment',
  templateUrl: './razorpaypayment.page.html',
  styleUrls: ['./razorpaypayment.page.scss'],
})
export class RazorpaypaymentPage implements OnInit {
  paymentAmount = 300;
  currency = 'INR';
  currencyIcon = '₹';
  razorKey = 'rzp_test_1DP5mmOlF5G5ag';
  cardDetails: any = {};
  constructor() { }

  ngOnInit() {
  }
  payWithRazor() {
    const options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: this.currency,
      key: this.razorKey,
      amount: this.paymentAmount * 100,
      name: 'foo',
      prefill: {
        email: 'admin@enappd.com',
        contact: '9621323231',
        name: 'Enappd'
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: () => {
          alert('dismissed');
        }
      }
    };

    const successCallback =  (paymentId) => {
      alert('payment_id: ' + paymentId);
    };

    const cancelCallback =  (error) => {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }
}
