import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProcedureDetailsPage } from './procedure-details.page';

const routes: Routes = [
  {
    path: '',
    component: ProcedureDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProcedureDetailsPage]
})
export class ProcedureDetailsPageModule {}
